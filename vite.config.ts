import fs from 'fs'

import react from '@vitejs/plugin-react'
import { certificateFor } from 'devcert'
import killPort from 'kill-port'
import { defineConfig, loadEnv, type ConfigEnv } from 'vite'

// https://vitejs.dev/config/
export default defineConfig(async ({ command, mode }) => {
  console.log(`command: ${command}, mode: ${mode}`)
  let https = undefined
  const port = 3000
  if (command === 'serve') {
    https = await certificateFor(['localhost', 'dev.onestepprojects.org'])
    await killPort(port)
  }

  return {
    define:
      mode === 'production' ? { 'process.env': 'window.env' } : getProcessEnv({ command, mode }),

    envPrefix: 'REACT_APP_',

    esbuild: {
      loader: 'tsx',
      include: /src\/.+\.(j|t)sx?$/,
      exclude: [/node_modules/],
    },

    optimizeDeps: {
      esbuildOptions: {
        plugins: [
          /**
           * Plugin to force trust `.js` as `.jsx`.
           *
           * @see https://github.com/vitejs/vite/discussions/3448#discussioncomment-749919
           */
          {
            name: 'load-js-files-as-jsx',
            setup(build) {
              build.onLoad({ filter: /src\/.+\.js$/ }, (args) => ({
                loader: 'tsx',
                contents: fs.readFileSync(args.path, 'utf8'),
              }))
            },
          },
        ],
      },
    },

    plugins: [react()],

    resolve: {
      alias: [
        /** Replace webpack css importing prefix. */
        { find: /^~(.+)/, replacement: '$1' },
        /**
         * Fix amplify build
         *
         * @see https://ui.docs.amplify.aws/react/getting-started/troubleshooting
         */
        {
          find: './runtimeConfig',
          replacement: './runtimeConfig.browser',
        },
      ],
    },

    server: {
      https,
      port,
      strictPort: true,
    },
  }
})

/**
 * Pick env vars which defined for react-scripts.
 */
function getProcessEnv({ mode }: ConfigEnv) {
  const reactAppEnv = loadEnv(mode, '.', 'REACT_APP_')

  reactAppEnv.DEBUG ??= undefined

  if (Object.keys(reactAppEnv).length) {
    return Object.fromEntries(
      Object.entries(reactAppEnv).map(([key, value]) => [
        `process.env.${key}`,
        JSON.stringify(value),
      ])
    )
  }
}
