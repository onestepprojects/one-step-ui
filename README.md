# One Step Main UI

## Getting Started

Install npm packages.

```shell
npm i
```

Start dev server.

```shell
npm start
```

Link a module. This will allow us to test the module locally. e.g. `organization-module`

```shell
npm link ../organization-module/frontend
npm start
```

```shell
cd organization-module/frontend
npm start
```

<details>
  <summary markdown="span">Start dev server with a valid https certificate.</summary>

So, running a local HTTPS server usually sucks. There's a range of approaches, each with their own tradeoff. The common one, using self-signed certificates, means having to ignore scary browser warnings for each project. The script below makes the process easy. Want a private key and certificate file to use with your server? Just type:

`HTTPS=1 npm start`

or

`npm start -- --env HTTPS=true`

You'll likely see password prompts from your OS at this point to authorize the new root CA.

Learn more [devcert - Development SSL made easy](https://github.com/davewasmer/devcert).

</details>

## Creating a Module

In order to create a module from your React Application, you will need to have an exported default module that represents your top-level module. In the case of the Health Module, this was created in [`src/index.js`](https://gitlab.com/onestepprojects/health-module/-/blob/main/frontend/src/index.js) and represented as ‘Health’ which is exported as a default functional component. Do not include navigation bars or side bars in your exported component. These will be included in the main UI app. Your component will be included in the center “content” section. This file should also include any routing and top-level styles. Your exported component should also expect to be passed router props that can be used to create sub-routes within your component from the main UI.

Next, you will need to include the necessary fields in your package.json file as described by the npm website under ‘[Create a package.json file](https://docs.npmjs.com/creating-node-js-modules#create-a-packagejson-file)’. These fields include the name and version. Below is the `package.json` file for the Health Module. You should also create a scoped module under @onestepprojects in order to communicate with the Gitlab group. Make sure that the “main”, “module”, and “source” fields are correct for your project. You do not need to use the same bundler.

```json
{
  "name": "@onestepprojects/health-module",
  "version": "1.0.1",
  "description": "OneStep Health Module",
  "author": "",
  "license": "MIT",
  "repository": "",
  "main": "dist/index.cjs",
  "module": "dist/index.js",
  "source": "src/index.ts",
  "types": "src/index.ts",
  "style": "dist/index.css",
  "engines": {
    "node": ">=18"
  },
  "scripts": {
    "build": "vite build",
    "lint": "eslint 'src/**/*.{ts,tsx,js,jsx}'",
    "lint:fix": "eslint 'src/**/*.{ts,tsx,js,jsx}' --fix",
    "prepublishOnly": "vite build",
    "semantic-release": "semantic-release",
    "start": "vite build --watch",
    "test": "jest",
    "test:watch": "jest --watch"
  },
  "devDependencies": {
    "@onestepprojects/frontend-infrastructure": "^2.5.0"
    // ...
  },
  "files": ["dist"],
  "dependencies": {
    // ...
  }
}
```

A `.gitlab-ci.yml` file should be created in order to build, test, and package the module when new changes are pushed to main. Below is the [config file](https://gitlab.com/onestepprojects/health-module/-/blob/main/.gitlab-ci.yml) for the Health Module. You may need to change details in this file including how tests are run. Our prepare step only gets run on tagged branches in Gitlab.

```yaml
stages:
  - build
  - test
  - release

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - frontend/node_modules/
    - .npm/

build-ui:
  stage: build
  image: node
  script:
    - echo "Building UI into dist/"
    - cd frontend/src/
    - npm install
    - npm run build

test-ui:
  stage: test
  image: node
  script:
    - cd frontend/src/
    - npm run test:unit
  artifacts:
    when: always
    reports:
      junit: frontend/junit.xml
  allow_failure: true

.test-e2e:
  stage: test
  image: cypress/base:10
  script:
    - echo "end to end testing"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - cypress/screenshots
      - cypress/videos
    reports:
      junit:
        - results/TEST-*.xml

.test-backend:
  stage: test
  image: maven:3.6.3-openjdk-15
  script:
    - echo "testing for backend"

release-ui:
  stage: release
  image: node:14-buster
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - |
      {
        echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
        echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
        echo "unsafe-perm = true"
      } | tee --append frontend/.npmrc
  script:
    - echo "release ui package"
    - cd frontend/
    - npm run semantic-release

.deploy-backend:
  stage: release
  image: maven:3.6.3-openjdk-15
  script:
    - echo "deploy backend package"
```

After running the pipeline for the first time, you will have a package available under “Package Registry” for your project.

## Adding a Module to the Main UI

Your module now needs to be added to the main UI app. Please continue to work off branches when adding or updating your module in the main UI app.

First you will need to create a Personal Access Token in Gitlab in order to install packages from the Gitlab registry. Follow the instructions [here](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry) for an instance-level npm endpoint.

On your new branch in the main UI app, run `npm install @onestepprojects/<package_name>`. Your package (with your default export) should now be available to the project. Import the component in `src/routes.js` and add a new route that will point to your component. You may also need to import your compiled css in `App.js`.

### Environment Variables

For development, Environment variables can be included in a file in the root of the project called `./.env.development`. The current development version of the environment file will be stored in the UI Channel in slack.

### UI Deployment

The UI run in an EKS cluster as a deployment using helm. Deployment files may be found in `deployment/`. The `deployment/` directory contains helm template files for a ConfigMap, Deployment, Ingress, and Service. If you must add environment variables to the application, please add them to the deployment. Secrets should be added as Kubernetes Secrets in the EKS cluster. Please reach out to Michaela DeForest if you must add a secret. Plain text environment variables may be added to the existing config map.

### Module Development Using the Main UI With Docker

You must be logged in to Gitlab Container Registry, which requires a Personal access token:

`docker login registry.gitlab.com -u <username> -p <token>`

Run pull command to get main ui image. Use the tags that are labeled as \*-develop. See (https://gitlab.com/onestepprojects/one-step-ui/container_registry/2746567)

`docker pull registry.gitlab.com/onestepprojects/one-step-ui:<dev-tag>`

Run the container with your module mounted into the container at the dist folder. The container should be accessible at (https://localhost:3000) and changes to your module should show up on refresh of the page, if mounted properly with the absolute path to your module. You may need to increase the memory allocated to docker, if you get an exit code error of 137.

`docker run --rm -p 3000:3000 -v /absolute/path/to/module/dist/:/app/node_modules/@onestepprojects/<module_name>/dist registry.gitlab.com/onestepprojects/one-step-ui:<dev-tag>`
