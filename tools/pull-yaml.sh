#!/bin/bash

# usage: tools/pull-yaml.sh <prod|staging|capstone>
# output can be redirected to yaml file for helm deployment
# requires GITLAB_TOKEN variable to be set in environment

all_environments=("main" "staging" "capstone")

environments() {
  if [ "$1" = "main" ]; then
    environment="main"
  elif [ "$1" = "staging" ]; then
    environment="staging"
  elif [ "$1" = "capstone-main" ]; then
    environment="capstone"
  fi
}

branch=$(git rev-parse --abbrev-ref HEAD || "main")
environments $branch

if [ $# -ne 0 ]; then
  found=0
  for a in "${all_environments[@]}"; do
    [[ "$a" = "$1" ]] && found=1
  done

  if [ $found -eq 1 ]; then
    environment="${1}"
  fi
fi

KEY="HELM_VALUES_$environment"

curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/onestepprojects%2Fone-step-ui/variables" | jq -r --arg KEY "$KEY" '.[] | select(.key==$KEY).value'
