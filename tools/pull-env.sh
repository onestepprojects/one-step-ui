#!/bin/bash

# usage: tools/pull-env.sh <prod|staging|capstone>
# output can be redirected to env file
# requires GITLAB_TOKEN variable to be set in environment

environment_yaml=$(tools/pull-yaml.sh $1)
environment_yaml=$(tail -n +4 <<<"${environment_yaml}")
environment_yaml=$(sed 's/^[ \t]*//' <<<"${environment_yaml}")

sed 's/: /=/' <<<"${environment_yaml}"
