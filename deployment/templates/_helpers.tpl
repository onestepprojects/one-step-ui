{{- define "imagePullSecrets" -}}
{{- if .Values.imagePullSecrets -}}
imagePullSecrets:
{{ toYaml .Values.imagePullSecrets }}
{{- end -}}
{{- end }}

{{- define "convertEnvToJson" -}}
{{- if . -}}
env.js: |
  window.env = {
    {{- trimSuffix "," ( include "jsonLoop" . ) }}
  }
{{- end }}
{{- end }}

{{- define "jsonLoop" -}}
{{- range $key, $value := . }}
{{- printf "  %s: %s," $key ($value | quote) | nindent 2 }}
{{- end }}
{{- end }}
