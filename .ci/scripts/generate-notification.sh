#!/bin/sh

values_file="${CI_COMMIT_BRANCH}"

if [ "$values_file" = "main" ]; then
  values_file="core"
fi

host=$(yq eval '.ingress.host' deployment/values/${values_file}.yaml)

read -r -d '' json_body << EOF
{
	"blocks": [
		{
			"type": "header",
			"text": {
				"type": "plain_text",
				"text": "A new version of the One Step Platform has been released!  :tada: :tada: :tada:",
				"emoji": true
			}
		},
		{
			"type": "actions",
			"elements": [
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "Open Platform",
						"emoji": true
					},
					"url": "https://${host}",
					"action_id": "actionId-0"
				}
			]
		},
		{
			"type": "actions",
			"elements": [
				{
					"type": "button",
					"text": {
						"type": "plain_text",
						"text": "View Release in Gitlab",
						"emoji": true
					},
					"url": "${CI_PROJECT_URL}/-/releases/v${VERSION}",
					"action_id": "actionId-0"
				}
			]
		},
		{
			"type": "context",
			"elements": [
				{
					"type": "plain_text",
					"text": "Environment: ${RELEASE_CHANNEL}",
					"emoji": true
				}
			]
		},
		{
			"type": "context",
			"elements": [
				{
					"type": "plain_text",
					"text": "Release: ${VERSION}",
					"emoji": true
				}
			]
		}
	]
}
EOF

echo "Notification Body:"
echo "${json_body}"

curl -X POST -H 'Content-type: application/json' --data "${json_body}" ${SLACK_WEBHOOK_URL}