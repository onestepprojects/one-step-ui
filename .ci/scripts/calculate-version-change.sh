#!/bin/bash

prev_version=$(cat package.json | jq -r --arg full_path "@onestepprojects/${REGISTRY_PATH}" '.dependencies[$full_path]' | sed 's/\^//g; s/\~//g')
IFS='.' read -r -a prev_array <<< "$prev_version"

npm install "@onestepprojects/${REGISTRY_PATH}@${VERSION_TAG}" >/dev/null 2>&1

new_path=$(npm list @onestepprojects/${REGISTRY_PATH}@${VERSION_TAG} -p -l)
new_version=$(echo "${new_path}" | sed 's/.*:@onestepprojects\/.*@//g')

if [ "${VERSION_TAG}" = "latest" ]; then
  echo "fix(module): Update @${REGISTRY_PATH} to version ${new_version} [ci skip]"
  exit 0
fi

IFS='.' read -r -a new_array <<< "$new_version"

version_type=""

if [[ "${#new_array[@]}" -eq "3" ]] && [[ "${#prev_array[@]}" -eq "4" ]]; then
  version_type="feat(module)"
elif [[ ${new_array[0]} -gt ${prev_array[0]} ]]; then
  version_type="perf(module)"
elif [[ ${new_array[1]} -gt ${prev_array[1]} ]]; then
  version_type="feat(module)"
elif [[ ${new_array[2]} -gt ${prev_array[2]} ]]; then
  version_type="fix(module)"
elif [[ ${new_array[3]} -gt ${prev_array[3]} ]]; then
  version_type="fix(module)"
fi

if [ -z "${version_type}" ]; then
  exit 0
fi

echo "${version_type}: Update @${REGISTRY_PATH} to version ${new_version} [ci skip]"
