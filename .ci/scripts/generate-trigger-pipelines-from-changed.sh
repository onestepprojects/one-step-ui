#!/bin/bash

group_images=$(curl -s --request GET --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${CI_API_V4_URL}/groups/${CI_PROJECT_ROOT_NAMESPACE}/registry/repositories?tags=true")

last_tag=$(git describe --tags)

if [[ $last_tag  =~ .*-?.*-g.{7} ]]; then
  to_compare="${last_tag}"
  last_tag="HEAD"
else
  to_compare=$(git describe --abbrev=0 --tags `git rev-list --tags --skip=1 --max-count=1`)
fi

messages=$(git log --oneline ${to_compare}..${last_tag} | awk '{print substr($0,index($0,$2))}')

declare -a modules

while IFS= read -r message; do
  if [[ $message =~ .*\(module\).*@([^[:space:]]*).*version\ ([^\ ]+)\ .* ]]; then
    module="${BASH_REMATCH[1]}"
    version="${BASH_REMATCH[2]}"
    project_id=$(echo ${group_images} | jq -r ".[] | select(.path? | contains(\"${module}\"))" | jq -r '.project_id' | head -n 1)
    modules["${project_id}"]=${version}
  fi
done <<< "$messages"

cat << EOF
stages:
- trigger

EOF

if [[ ${#modules[@]} -le 0 ]]; then
  cat << EOF
no-trigger:
  stage: trigger
  script: echo "nothing to trigger"
EOF
  exit 0
fi

for line in "${!modules[@]}"; do
  project_path=$(curl -s --request GET --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${CI_API_V4_URL}/projects/${line}" | jq -r '.path_with_namespace')
  project_path_name=$(echo "${project_path#*\/}" | tr -d '"')

  cat << EOF
trigger-service-${project_path_name}:
  stage: trigger
  variables:
    ONLY_DEPLOY: "true"
    VERSION: ${modules[$line]}
    VALUES_FILE_NAME: staging
  trigger: 
    project: ${project_path}
    branch: main

EOF
done

