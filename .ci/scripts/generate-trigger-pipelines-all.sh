#!/bin/bash

MODULES_TO_SKIP=("accreditation-admin-module" "authentication-helper" "education-module" "solutions-library" "ticket-module" )

used_packages=$(cat package.json | jq -r '.dependencies | to_entries[] | select(.key | startswith("@onestepprojects")) | .key')
num_used_packages=$(echo "${used_packages}" | wc -l)

cat << EOF
stages:
- trigger

EOF

if [ "${num_used_packages}" = "0" ]; then
  cat << EOF
no-trigger:
  stage: trigger
  script: echo "nothing to trigger"
EOF
  exit 0
fi

while IFS= read -r line; do
  project_path=$(curl -s --request GET --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${CI_API_V4_URL}/groups/${CI_PROJECT_ROOT_NAMESPACE}/packages?package_name=${line}" | jq -r '.[0]._links.web_path')
  project_path="${project_path:1}"
  project_path="${project_path%/-*}"
  module_name="${line#'@onestepprojects/'}"

  if [[ " ${MODULES_TO_SKIP[*]} " =~ " ${module_name} " ]]; then
    continue
  fi

  module=""

  if [[ "$module_name" = "accreditation-module" ]]; then
    module="MODULE: accreditation"
  fi

  version=$(cat package-lock.json | jq -r ".dependencies.\"${line}\".version")

  cat << EOF
trigger-service-${module_name}:
  stage: trigger
  variables:
    ONLY_DEPLOY: "true"
    VERSION: ${version}
    VALUES_FILE_NAME: prod
    ${module}
  trigger: 
    project: ${project_path}
    branch: main

EOF
done <<< "${used_packages}"