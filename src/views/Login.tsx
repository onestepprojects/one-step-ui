import { Auth } from 'aws-amplify'
import { useEffect, type FC } from 'react'

/** Redirect to login page. */
const Login: FC = () => {
  useEffect(() => {
    Auth.federatedSignIn()
  })
  return null
}

export default Login
