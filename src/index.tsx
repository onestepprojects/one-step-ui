import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'

import { BrowserRouter as Router } from 'react-router-dom-v5'

import { urlSafeDecode } from '@aws-amplify/core'

import App from './App'

import { amplifyHost } from './data/aws-amplify.config'
import * as serviceWorker from './serviceWorker'

// Redirect to amplify if login from amplify
const state = new URLSearchParams(location.search).get('state')
if (state && /-/.test(state)) {
  const customState = state.split('-').splice(1).join('-')
  const decodedState = urlSafeDecode(customState)
  const parsedState = JSON.parse(decodedState)
  if (parsedState.amplifyApp) {
    const url = new URL(location.href)
    url.host = `${parsedState.amplifyApp}${amplifyHost}`
    location.href = String(url)
  }
}

/** The theme should be compatible with Ant Design's theme token */
const theme = {
  /** Primary color */
  colorPrimary: '#007bff',
}

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Router>
      <App />
    </Router>
  </ThemeProvider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
