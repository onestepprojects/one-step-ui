declare module "@onestepprojects/authentication-helper" {
  const value: {
    AuthHelper: any;
  };
  export = value;
}

declare module "@onestepprojects/fund-module" {
  const value: any;
  export = value;
}

declare module "@onestepprojects/health-module" {
  const value: any;
  export = value;
}

declare module "@onestepprojects/solutions-library" {
  const value: any;
  export = value;
}

declare module "@onestepprojects/resource-module" {
  const value: any;
  export = value;
}

declare module "@onestepprojects/organization-module" {
  const value: any;
  export = value;
}

declare module "@onestepprojects/survey-module" {
  const value: any;
  export = value;
}
