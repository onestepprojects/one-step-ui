// https://stackoverflow.com/a/53981706/439964
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      PUBLIC_URL: string;
      NODE_ENV: 'development' | 'production';
    }
  }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {}
