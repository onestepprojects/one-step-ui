import { CognitoUser } from 'amazon-cognito-identity-js'

export type User = CognitoUser | null;
export type Person = any;
