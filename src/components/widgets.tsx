import styled from 'styled-components'

export const NavLink = styled.a`
  display: block;
  color: ${(props) => props.theme.colorPrimary};
  font-size: 0.8125rem;
  font-weight: 400;
  padding: 0.625rem;
  transition: all 0.25s cubic-bezier(0.27, 0.01, 0.38, 1.06);

  &:hover {
    text-decoration: none;
  }

  &:visited {
    color: ${(props) => props.theme.colorPrimary};
  }
`

export const Navbar = styled.nav`
  display: flex;
  position: relative;
  justify-content: space-between;
  height: 3.75rem;
`
