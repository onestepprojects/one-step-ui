import React from "react";
import { NavLink } from "../../widgets";

const SidebarProfile: React.FC<{ id: string }> = ({ id }) => {
  return (
    <div className="nav-item">
      <NavLink href={`/persons/${id}`}>
        <div className="d-inline-block item-icon-wrapper">
          <i className="material-icons navbar-icon">person</i>
        </div>
        <span className="navbar-text">My Profile</span>
      </NavLink>
    </div>
  );
};

export default SidebarProfile;
